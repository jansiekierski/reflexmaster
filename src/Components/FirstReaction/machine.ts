import { createMachine, assign } from "xstate";

interface Context {
  status: "STARTED" | "STOPPED";
  score: number;
  scoresTable: number[];
  clickOnDiv: number;
  missclicks: number;
}

export type Event =
  | { type: "START" }
  | { type: "STOP" }
  | { type: "CLICK" }
  | { type: "CLICK_ON_DIV" };
export const firstReactionMachine = createMachine(
  {
    initial: "init",
    schema: { context: {} as Context, events: {} as Event },
    context: {
      status: "STOPPED",
      score: 0,
      scoresTable: [],
      clickOnDiv: 0,
      missclicks: 0,
    },
    states: {
      init: {
        on: {
          START: { actions: ["updateStatus", "resetScores"] },
          CLICK_ON_DIV: { actions: "countClickOnDiv" },
          CLICK: { actions: "countClicks" },
          STOP: {
            actions: ["setScoreTable", "countMissClicks", "updateStatus"],
          },
        },
      },
    },
  },
  {
    actions: {
      updateStatus: assign<Context, Event>({
        status: (context, event) => {
          if (event.type === "START" && context.status === "STOPPED") {
            return "STARTED";
          } else {
            return "STOPPED";
          }
        },
      }),
      countClicks: assign<Context, Event>({
        score: (context, event) => {
          if (event.type === "CLICK") {
            return context.score + 1;
          }
          return context.score;
        },
      }),
      countClickOnDiv: assign<Context, Event>({
        clickOnDiv: (context, event) => {
          if (event.type === "CLICK_ON_DIV") {
            return context.clickOnDiv + 1;
          }
          return context.clickOnDiv;
        },
      }),
      setScoreTable: assign<Context, Event>({
        scoresTable: (context, event) => {
          if (event.type === "STOP") {
            return [...context.scoresTable, context.score];
          }
          return context.scoresTable;
        },
      }),
      resetScores: assign<Context, Event>({
        score: (_, __) => {
          return 0;
        },
        clickOnDiv: (_, __) => {
          return 0;
        },
      }),
      countMissClicks: assign<Context, Event>({
        missclicks: (context, _) => {
          return context.clickOnDiv - context.score - 1;
        },
      }),
    },
  }
);
