import { useMachine } from "@xstate/react";
import React from "react";
import { firstReactionMachine } from "./machine";
import { Event as PublicEvents } from "../machine";

interface Props {
  sendMainMachine(event: PublicEvents): void;
  scoresTable: number[];
}
export default function FirstReaction({ sendMainMachine, scoresTable }: Props) {
  const [current, send] = useMachine(firstReactionMachine, {
    context: {
      scoresTable,
    },
  });
  const buttonRef = React.useRef<HTMLButtonElement>(null);
  React.useEffect(() => {
    sendMainMachine({
      type: "UPDATE_FIRST_SCORES",
      scoresTable: current.context.scoresTable,
    });
  }, [current.context.scoresTable, sendMainMachine]);
  function getRandomColor() {
    const letters = "0123456789ABCDEF".split("");
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.round(Math.random() * 15)];
    } //ends for loop
    return color;
  } // ends getRandomColor Function

  // eslint-disable-next-line react-hooks/exhaustive-deps
  function makeBox() {
    if (buttonRef.current === null) {
      return;
    }
    if (Math.random() > 0.5) {
      buttonRef.current.style.borderRadius = "50%";
    } else {
      buttonRef.current.style.borderRadius = "0";
    }

    let top = Math.random();
    top = top * 100;
    let left = Math.random();
    left = left * 200;

    buttonRef.current.style.top = top + "px";
    buttonRef.current.style.left = left + "px";

    buttonRef.current.style.backgroundColor = getRandomColor();

    buttonRef.current.style.display = "block";
  }

  const handleOnClick = React.useCallback(() => {
    makeBox();
    send({ type: "CLICK" });
  }, [makeBox, send]);

  const handleStart = React.useCallback(() => {
    if (current.context.status === "STOPPED") {
      send({ type: "START" });
    }
  }, [current.context.status, send]);

  const handleOnDivClick = React.useCallback(() => {
    send({ type: "CLICK_ON_DIV" });
  }, [send]);

  React.useEffect(() => {
    if (current.context.status === "STARTED") {
      const timer = setTimeout(() => {
        send({ type: "STOP" });
      }, 20000);
      //to do change to 20000
      return () => clearTimeout(timer);
    }
    // timeout to set
  }, [current.context.status, send]);
  return (
    <>
      <div className="text-2xl py-3.5 font-bold">First reaction test</div>

      <div
        onClick={handleOnDivClick}
        className="flex flex-col items-center justify-center  border-2 h-5/6"
      >
        {current.context.status === "STOPPED" && (
          <p className="text-2xl py-5 px-8 font-bold text-center">
            How many block can you click in 20s?{" "}
          </p>
        )}
        {current.context.status === "STOPPED" &&
        current.context.scoresTable.length < 10 ? (
          <button
            className="p-3.5 border-2 rounded bg-green-900 text-white"
            onClick={handleStart}
          >
            Let's START!
          </button>
        ) : (
          <div className="p-3.5" />
        )}
        {current.context.scoresTable.length === 10 && (
          <p className="text-2xl py-5 px-8 font-bold text-center">
            You've reached 10 free test, please buy full version.
          </p>
        )}

        {current.context.score !== 0 && current.context.status === "STOPPED" && (
          <>
            <p className="text-l pt-3.5 font-bold" id="printReactionTime">
              Your score is {current.context.score} boxes in 20s!
            </p>
            <p>You missclicked box: {current.context.missclicks} times</p>
            <p>
              Your accuracy:{" "}
              {(
                (current.context.score /
                  (current.context.missclicks + current.context.score)) *
                100
              ).toFixed(1)}{" "}
              %
            </p>
          </>
        )}
        {current.context.status !== "STOPPED" && (
          <button
            ref={buttonRef}
            onClick={handleOnClick}
            className="w-10 h-10 bg-amber-300 object-none relative"
            id="box"
          ></button>
        )}
      </div>
    </>
  );
}
