import { useMachine } from "@xstate/react";
import React from "react";
import { secondReactionMachine } from "./machine";
import { Event as PublicEvents } from "../machine";

interface Props {
  sendMainMachine(event: PublicEvents): void;
  scoresTable: number[];
}
export default function SecondReaction({
  sendMainMachine,
  scoresTable,
}: Props) {
  const [current, send] = useMachine(secondReactionMachine, {
    context: {
      scoresTable,
    },
  });

  React.useEffect(() => {
    sendMainMachine({
      type: "UPDATE_SECOND_SCORES",
      scoresTable: current.context.scoresTable,
    });
  }, [current.context.scoresTable, sendMainMachine]);

  const randomTime = Math.random() * 10000;
  const handleStart = React.useCallback(() => {
    send({ type: "START", status: "WAIT" });
  }, [send]);
  const handleClick = React.useCallback(() => {
    send({ type: "CLICK", status: "OK" });
  }, [send]);

  const handleWaiting = React.useCallback(() => {
    send({ type: "TOO_SOON", status: "TOO_SOON" });
  }, [send]);
  const handleTryAgain = React.useCallback(() => {
    send({ type: "TRY_AGAIN", status: "INIT" });
  }, [send]);

  React.useEffect(() => {
    const timer = setTimeout(() => {
      send({ type: "GOOD_TO_GO", status: "STOP" });
    }, randomTime);
    return () => clearTimeout(timer);
  }, [randomTime, send]);
  // https://humanbenchmark.com/tests/reactiontime/statistics random info about
  return (
    <>
      <div className="text-2xl py-3.5 font-bold">Second reaction test</div>
      <div className="flex flex-col items-center h-5/6 justify-center border-2 rounded">
        {current.matches("init") && (
          <div className="w-full h-full bg-neutral-400 flex flex-col items-center justify-center">
            <button className="w-full h-80" onClick={handleStart}>
              <h3 className="text-4xl font-bold text-white">
                Reaction Time Test
              </h3>
              <p className="text-xl font-bold text-white">
                When the red box turns green, click as quickly as you can.
              </p>
              <p className="text-xl font-bold text-white">
                Click here to start
              </p>
            </button>
          </div>
        )}
        {current.matches("wait") && (
          <div className="bg-red-900 w-full h-full flex flex-col items-center justify-center">
            <button className="w-full h-80 " onClick={handleWaiting}>
              <h3 className="text-4xl font-bold text-white">
                Wait for green...
              </h3>
            </button>
          </div>
        )}
        {current.matches("tooSoon") && (
          <div className="bg-red-900 w-full h-full flex flex-col items-center justify-center">
            <button className="w-full h-80 " onClick={handleTryAgain}>
              <h3 className="text-4xl font-bold text-white">Too soon...</h3>
              <p className="text-xl font-bold text-white">
                Click again and try one more time{" "}
              </p>
            </button>
          </div>
        )}
        {current.matches("goodToGo") && (
          <div className="w-full  h-full bg-green-600 flex flex-col items-center justify-center">
            <button className="w-full h-80 " onClick={handleClick}>
              <h3 className="text-4xl font-bold text-white">CLICK!!!</h3>
            </button>
          </div>
        )}
        {current.matches("success") && (
          <div className="w-full  h-full bg-green-800 flex flex-col items-center justify-center">
            <button className="w-full h-80" onClick={handleTryAgain}>
              <h3 className="text-3xl font-bold text-white">CONGRATS!</h3>
              {current.context.currentScore !== 0 && (
                <p className="text-4xl py-3 font-bold text-white">
                  {current.context.currentScore}ms
                </p>
              )}
              <p className="text-xl font-bold text-white">
                Click again and try again
              </p>
            </button>
          </div>
        )}
      </div>
    </>
  );
}
