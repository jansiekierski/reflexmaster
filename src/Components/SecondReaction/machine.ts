import { createMachine, assign } from "xstate";

export interface Context {
  status: "INIT" | "WAIT" | "TOO_SOON" | "OK" | "STOP";
  currentScore: number;
  scoresTable: number[];
  startTime: number;
  endTime: number;
}

export type Event =
  | { type: "START"; status: Context["status"] }
  | { type: "CLICK"; status: Context["status"] }
  | { type: "STOP"; status: Context["status"] }
  | { type: "TRY_AGAIN"; status: Context["status"] }
  | { type: "TOO_SOON"; status: Context["status"] }
  | { type: "GOOD_TO_GO"; status: Context["status"] };

export const secondReactionMachine = createMachine(
  {
    initial: "init",
    schema: {
      context: {} as Context,
      events: {} as Event,
    },
    context: {
      status: "INIT",
      currentScore: 0,
      scoresTable: [],
      startTime: 0,
      endTime: 0,
    },
    states: {
      init: {
        on: {
          START: {
            target: "wait",
            actions: "updateStatus",
          },
        },
      },
      wait: {
        on: {
          TOO_SOON: { target: "tooSoon", actions: "updateStatus" },
          GOOD_TO_GO: {
            target: "goodToGo",
            actions: ["setStartTime", "updateStatus"],
          },
        },
      },
      tooSoon: {
        on: { TRY_AGAIN: { target: "init", actions: "updateStatus" } },
      },
      goodToGo: {
        on: {
          CLICK: {
            target: "success",
            actions: [
              "setEndTime",
              "setCurrentScore",
              "setScoreTable",
              "updateStatus",
            ],
          },
        },
      },
      success: {
        on: {
          TRY_AGAIN: {
            target: "init",
            actions: "updateStatus",
          },
        },
      },
    },
  },
  {
    actions: {
      setStartTime: assign<Context, Event>({
        startTime: (context, event) => {
          if (event.type === "GOOD_TO_GO") {
            return new Date().getTime();
          }
          return context.startTime;
        },
      }),
      setEndTime: assign<Context, Event>({
        endTime: (context, event) => {
          if (event.type === "CLICK") {
            return new Date().getTime();
          }
          return context.endTime;
        },
      }),
      updateStatus: assign<Context, Event>({
        status: (context, event) => {
          if (
            event.type === "CLICK" ||
            event.type === "START" ||
            event.type === "STOP" ||
            event.type === "GOOD_TO_GO" ||
            event.type === "TRY_AGAIN"
          ) {
            return event.status;
          }
          return context.status;
        },
      }),
      setCurrentScore: assign<Context, Event>({
        currentScore: (context, event) => {
          if (event.type === "CLICK") {
            return context.endTime - context.startTime;
          }
          return context.currentScore;
        },
      }),
      setScoreTable: assign<Context, Event>({
        scoresTable: (context, event) => {
          if (event.type === "CLICK") {
            return [...context.scoresTable, context.currentScore];
          }
          return context.scoresTable;
        },
      }),
    },
  }
);
