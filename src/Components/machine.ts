import { createMachine, assign } from "xstate";

export type Step = "FIRST" | "SECOND" | "THIRD";
interface Context {
  step: Step;
  firstTestScores: number[];
  secondTestScores: number[];
  thirdTestScores: number[];
}
export type Event =
  | { type: "FIRST"; step: Step }
  | { type: "SECOND"; step: Step }
  | { type: "THIRD"; step: Step }
  | { type: "UPDATE_FIRST_SCORES"; scoresTable: number[] }
  | { type: "UPDATE_SECOND_SCORES"; scoresTable: number[] }
  | { type: "UPDATE_THIRD_SCORES"; scoresTable: number[] };

export const mainMachine = createMachine(
  {
    initial: "init",
    schema: { context: {} as Context, events: {} as Event },
    states: {
      init: {
        on: {
          FIRST: { actions: "setStep" },
          SECOND: { actions: "setStep" },
          THIRD: { actions: "setStep" },
          UPDATE_FIRST_SCORES: { actions: "updateFirstScore" },
          UPDATE_SECOND_SCORES: { actions: "updateSecondScore" },
          UPDATE_THIRD_SCORES: { actions: "updateThirdScore" },
        },
      },
    },
  },
  {
    actions: {
      setStep: assign<Context, Event>({
        step: (context, event) => {
          if (
            event.type === "FIRST" ||
            event.type === "SECOND" ||
            event.type === "THIRD"
          ) {
            return event.step;
          }
          return context.step;
        },
      }),
      updateFirstScore: assign<Context, Event>({
        firstTestScores: (context, event) => {
          if (event.type === "UPDATE_FIRST_SCORES") {
            return event.scoresTable;
          }
          return context.firstTestScores;
        },
      }),
      updateSecondScore: assign<Context, Event>({
        secondTestScores: (context, event) => {
          if (event.type === "UPDATE_SECOND_SCORES") {
            return event.scoresTable;
          }
          return context.secondTestScores;
        },
      }),
      updateThirdScore: assign<Context, Event>({
        thirdTestScores: (context, event) => {
          if (event.type === "UPDATE_THIRD_SCORES") {
            return event.scoresTable;
          }
          return context.thirdTestScores;
        },
      }),
    },
  }
);
