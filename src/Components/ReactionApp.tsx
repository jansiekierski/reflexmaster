import { useMachine } from "@xstate/react";
import React from "react";
import FirstReaction from "./FirstReaction";
import { mainMachine, Step } from "./machine";
import SecondReaction from "./SecondReaction";
import ThirdReaction from "./ThirdReaction";
import background from "../assets/forest.jpg";

export default function ReactionApp() {
  const [openTab, setOpenTab] = React.useState(1);
  const [current, send] = useMachine(mainMachine, {
    context: {
      step: "FIRST" as Step,
      firstTestScores: [],
      secondTestScores: [],
      thirdTestScores: [],
    },
  });

  const handleGoToFirstStep = React.useCallback(() => {
    return send({ type: "FIRST", step: "FIRST" });
  }, [send]);
  const handleGoToSecondStep = React.useCallback(() => {
    return send({ type: "SECOND", step: "SECOND" });
  }, [send]);
  const handleGoToThirdStep = React.useCallback(() => {
    return send({ type: "THIRD", step: "THIRD" });
  }, [send]);

  return (
    <div className="flex h-full mt-8">
      <div className="flex flex-col justify-center align-center h-full w-1/2 border-r">
        <div
          className="w-11/12 rounded-lg text-white bg-cover "
          style={{
            backgroundImage: `url(${background})`,
          }}
        >
          <div className="rounded-lg w-full h-full opacity-60 bg-black">
            <h3 className="text-white font-bold text-l p-2 text-center">
              It's your 10 last's scores in {current.context.step} Reaction Test
            </h3>
            <div className="h-96  rounded-lg opacity-60 bg-black w-full">
              <table className="w-full ">
                <thead>
                  <tr>
                    <th className="p-2">Round:</th>
                    <th className="p-2">Score:</th>
                  </tr>
                </thead>
                <tbody>
                  {current.context.step === "FIRST" &&
                    current.context.firstTestScores.map((item, index) => {
                      if (index === 9) {
                        return (
                          <tr className="text-center">
                            <td>You've reached 10 free test </td>
                            <td>please buy full version.</td>.
                          </tr>
                        );
                      }
                      return (
                        <tr className="text-center">
                          <td className="pl-2 text-center">{index + 1}</td>
                          <td>{item} boxes</td>
                        </tr>
                      );
                    })}
                  {current.context.step === "SECOND" &&
                    current.context.secondTestScores.map((item, index) => {
                      if (index === 9) {
                        return (
                          <tr className="text-center">
                            <td>You've reached 10 free test </td>
                            <td>please buy full version.</td>
                          </tr>
                        );
                      }
                      return (
                        <tr className="text-center">
                          <td className="pl-2 ">{index + 1}</td>
                          <td>{item} ms</td>
                        </tr>
                      );
                    })}
                  {current.context.step === "THIRD" &&
                    current.context.thirdTestScores.map((item, index) => {
                      if (index === 9) {
                        return (
                          <tr className="text-center">
                            <td>You've reached 10 free test </td>
                            <td>please buy full version.</td>
                          </tr>
                        );
                      }
                      return (
                        <tr className="text-center">
                          <td className="pl-2">{index + 1}</td>
                          <td>{item} ms</td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-col h-full w-1/2 ml-8">
        <div className="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
          <ul className="flex flex-wrap -mb-px">
            <li
              id="first"
              className={
                "mr-2 inline-block p-4 rounded-t-lg border-b-2" +
                (openTab === 1
                  ? " text-blue-600 border-emerald-600 active dark:text-emerald-500 dark:border-emerald-500"
                  : "border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300")
              }
            >
              <button
                onClick={() => {
                  setOpenTab(1);
                  handleGoToFirstStep();
                }}
              >
                First
              </button>
            </li>
            <li
              id="second"
              // aria-current="step"
              className={
                "mr-2 inline-block p-4 rounded-t-lg border-b-2" +
                (openTab === 2
                  ? " text-emerald-600 border-emerald-600 active dark:text-emerald-500 dark:border-emerald-500"
                  : "border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300")
              }
            >
              <button
                onClick={() => {
                  setOpenTab(2);
                  handleGoToSecondStep();
                }}
              >
                Second
              </button>
            </li>
            <li
              id="third"
              className={
                "mr-2 inline-block p-4 rounded-t-lg border-b-2" +
                (openTab === 3
                  ? " text-emerald-600 border-emerald-600 active dark:text-emerald-500 dark:border-emerald-500"
                  : "border-transparent hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300")
              }
            >
              <button
                onClick={() => {
                  setOpenTab(3);
                  handleGoToThirdStep();
                }}
              >
                Third
              </button>
            </li>
          </ul>
        </div>
        <div className="pt-4 h-full">
          {current.context.step === "FIRST" && (
            <FirstReaction
              sendMainMachine={send}
              scoresTable={current.context.firstTestScores}
            />
          )}
          {current.context.step === "SECOND" && (
            <SecondReaction
              sendMainMachine={send}
              scoresTable={current.context.secondTestScores}
            />
          )}
          {current.context.step === "THIRD" && (
            <ThirdReaction
              sendMainMachine={send}
              scoresTable={current.context.thirdTestScores}
            />
          )}
        </div>
      </div>
    </div>
  );
}
