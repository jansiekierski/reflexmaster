import { useMachine } from "@xstate/react";
import React from "react";
import { thirdReactionMachine } from "./machine";
import { Event as PublicEvents } from "../machine";

interface Props {
  sendMainMachine(event: PublicEvents): void;
  scoresTable: number[];
}
export default function ThirdReaction({ sendMainMachine, scoresTable }: Props) {
  const [current, send] = useMachine(thirdReactionMachine, {
    context: {
      scoresTable,
    },
  });
  const buttonRef = React.useRef<HTMLButtonElement>(null);

  React.useEffect(() => {
    sendMainMachine({
      type: "UPDATE_THIRD_SCORES",
      scoresTable: current.context.scoresTable,
    });
  }, [current.context.scoresTable, sendMainMachine]);

  function getRandomColor() {
    const letters = "0123456789ABCDEF".split("");
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.round(Math.random() * 15)];
    } //ends for loop
    return color;
  } // ends getRandomColor Function

  // eslint-disable-next-line react-hooks/exhaustive-deps
  function makeBox() {
    if (buttonRef.current === null) {
      return;
    }
    if (Math.random() > 0.5) {
      buttonRef.current.style.borderRadius = "50%";
    } else {
      buttonRef.current.style.borderRadius = "0";
    }

    let top = Math.random();
    top = top * 100;
    let left = Math.random();
    left = left * 200;

    buttonRef.current.style.top = top + "px";
    buttonRef.current.style.left = left + "px";

    buttonRef.current.style.backgroundColor = getRandomColor();

    buttonRef.current.style.display = "block";
  }

  const handleOnClick = React.useCallback(() => {
    makeBox();
    send({ type: "CLICK" });
  }, [makeBox, send]);
  const handleOnDivClick = React.useCallback(() => {
    send({ type: "CLICK_ON_DIV" });
  }, [send]);

  const handleStart = React.useCallback(() => {
    if (current.context.status === "STOPPED") {
      send({ type: "START" });
    }
  }, [current.context.status, send]);
  React.useEffect(() => {
    if (current.context.score === 10) {
      send({ type: "STOP" });
    }
  }, [current.context.score, send]);

  return (
    <>
      <div className="text-2xl py-3.5 font-bold">Third reaction test</div>
      <div
        onClick={handleOnDivClick}
        className="flex flex-col items-center justify-center border-2 h-5/6"
      >
        {current.context.status === "STOPPED" && (
          <p className="text-xl py-5 px-8 font-bold text-center">
            Click on the 10 boxes and circles as quickly as you can - check your
            time!
          </p>
        )}
        {current.context.status === "STOPPED" && (
          <button
            className="p-3.5 border-2 rounded bg-green-900 text-white"
            onClick={handleStart}
          >
            Let's START!
          </button>
        )}

        {current.context.totalTime !== 0 &&
          current.context.status === "STOPPED" && (
            <>
              <p className="text-l pt-3.5 font-bold" id="printReactionTime">
                It took {current.context.totalTime}ms to click in 10 boxes
              </p>
              <p>You missclicked box: {current.context.missclicks} times</p>
            </>
          )}
        {current.context.status !== "STOPPED" && (
          <button
            ref={buttonRef}
            onClick={handleOnClick}
            className="w-10 h-10 bg-amber-300 object-none relative"
            id="box"
          ></button>
        )}
      </div>
    </>
  );
}
