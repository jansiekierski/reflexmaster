import { createMachine, assign } from "xstate";

interface Context {
  status: "STARTED" | "STOPPED";
  totalTime: number;
  startTime: number;
  endTime: number;
  score: number;
  scoresTable: number[];
  clickOnDiv: number;
  missclicks: number;
}

export type Event =
  | { type: "START" }
  | { type: "STOP" }
  | { type: "CLICK" }
  | { type: "CLICK_ON_DIV" };
export const thirdReactionMachine = createMachine(
  {
    initial: "init",
    schema: { context: {} as Context, events: {} as Event },
    context: {
      status: "STOPPED",
      totalTime: 0,
      startTime: 0,
      endTime: 0,
      score: 0,
      scoresTable: [],
      clickOnDiv: 0,
      missclicks: 0,
    },
    states: {
      init: {
        on: {
          START: { actions: ["setStartTime", "updateStatus", "resetScores"] },
          CLICK_ON_DIV: { actions: "countClickOnDiv" },
          CLICK: { actions: "countClicks" },
          STOP: {
            actions: [
              "setEndTime",
              "setCurrentScore",
              "setScoreTable",
              "countMissClicks",
              "updateStatus",
            ],
          },
        },
      },
    },
  },
  {
    actions: {
      updateStatus: assign<Context, Event>({
        status: (context, event) => {
          if (event.type === "START" && context.status === "STOPPED") {
            return "STARTED";
          } else {
            return "STOPPED";
          }
        },
      }),
      countClicks: assign<Context, Event>({
        score: (context, event) => {
          if (event.type === "CLICK") {
            return context.score + 1;
          }
          return context.score;
        },
      }),
      setScoreTable: assign<Context, Event>({
        scoresTable: (context, event) => {
          if (event.type === "STOP") {
            return [...context.scoresTable, context.totalTime];
          }
          return context.scoresTable;
        },
      }),
      setStartTime: assign<Context, Event>({
        startTime: (context, event) => {
          if (event.type === "START") {
            return new Date().getTime();
          }
          return context.startTime;
        },
      }),
      setEndTime: assign<Context, Event>({
        endTime: (context, event) => {
          if (event.type === "STOP") {
            return new Date().getTime();
          }
          return context.endTime;
        },
      }),

      countClickOnDiv: assign<Context, Event>({
        clickOnDiv: (context, event) => {
          if (event.type === "CLICK_ON_DIV") {
            return context.clickOnDiv + 1;
          }
          return context.clickOnDiv;
        },
      }),

      setCurrentScore: assign<Context, Event>({
        totalTime: (context, event) => {
          if (event.type === "STOP") {
            return context.endTime - context.startTime;
          }
          return context.totalTime;
        },
      }),
      resetScores: assign<Context, Event>({
        score: (_, __) => {
          return 0;
        },
        clickOnDiv: (_, __) => {
          return 0;
        },
      }),
      countMissClicks: assign<Context, Event>({
        missclicks: (context, _) => {
          return context.clickOnDiv - context.score - 1;
        },
      }),
    },
  }
);
