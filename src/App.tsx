import React from "react";
import ReactionApp from "./Components/ReactionApp";

function App() {
  return (
    <div className="flex flex-col h-screen max-w-screen-xl w-full mx-auto">
      <h1 className="p-8 text-6xl font-bold opacity-2 border-b">
        REACTION APP.
      </h1>
      <ReactionApp />
    </div>
  );
}

export default App;
