module.exports = {
  theme: {
    extend: {
      backgroundImage: {
        forest: "url('/img/forest.jpg')",
        green: "url('/img/green.jpg')",
      },
    },
  },
  plugins: [],
  content: ["./src/**/*.{js,jsx,ts,tsx,html}", "./public/index.html"],
  quickSuggestions: {
    strings: true,
  },
  includeLanguages: {
    javascript: "javascript",
    html: "HTML",
    typescriptreact: "html",
  },
};
